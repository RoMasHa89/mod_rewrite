<?php
// Хост (обычно localhost)
$db_host = "localhost";
// Имя базы данных
$db_name = "mod_rewrite";
// Логин для подключения к базе данных
$db_user = "mod_rewrite";
// Пароль для подключения к базе данных
$db_pass = "mod_rewrite";


$db = mysqli_connect ($db_host, $db_user, $db_pass, $db_name) or die ("Connection Error");

// Указываем кодировку, в которой будет получена информация из базы
@mysqli_query ($db, 'set character_set_results = "utf8"');


if(!mysqli_query($db,"DESCRIBE images")) {
    // NOT Exists
    mysqli_query($db, "CREATE TABLE images (id int(6) NOT NULL auto_increment,img_name varchar(15) NOT NULL,view_count int(6) NOT NULL,PRIMARY KEY (id),UNIQUE id (id),KEY id_2 (id))");
}

function inc_image_hits($db, $name) {
    $query = mysqli_query($db, "SELECT img_name FROM images WHERE img_name = '$name'");

    if (!$query->num_rows) {
        mysqli_query($db, "INSERT INTO images SET img_name='$name'");
        mysqli_query($db, "UPDATE images SET view_count=1 WHERE img_name='$name'");
    } else {
        mysqli_query($db, "UPDATE images SET view_count=view_count+1 WHERE img_name='$name'");
        $result = mysqli_query($db, "SELECT view_count FROM images WHERE img_name = '$name'");
        $row = mysqli_fetch_assoc($result);
        return $row['view_count'];
    }

    return 1;
}

function get_image_hits($db, $name) {
    $query = mysqli_query($db, "SELECT img_name FROM images WHERE img_name = '$name'");
    if ($query->num_rows) {
        $result = mysqli_query($db, "SELECT view_count FROM images WHERE img_name = '$name'");
        $row = mysqli_fetch_assoc($result);
        return $row['view_count'];
    }

    return -1;
}

?>