<?php
include 'db.php';
?>
<html>
<head>
    <style>
        .thumbnail {
            position: relative;
            width: 200px;
            height: 200px;
            overflow: hidden;
        }
        .thumbnail img {
            position: absolute;
            left: 50%;
            top: 50%;
            width: 100%;
            height: auto;
            -webkit-transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
            transform: translate(-50%,-50%);
        }
    </style>
</head>

<body>
    <div class="thumbnail">
        <a href="image-123.html">Image 123 (<?php echo get_image_hits($db,'123') ?> Hits)
            <img src="thumbnail-123.jpg" alt="Image" />
        </a>
    </div>
    <div class="thumbnail">
        <a href="image-345.html">Image 345 (<?php echo get_image_hits($db,'345') ?> Hits)
            <img src="thumbnail-345.jpg" alt="Image" />
        </a>
    </div>
</body>
</html>
